/*Copyright [2021] [ENRIQUE COLLADO MUÑOZ]
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing,
software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
implied.
See the License for the specific language governing permissions
and
limitations under the License.*/
package mates;
import java.util.Scanner;
import java.util.Random;

public class Matematicas{
	/**
	 * Genera una aproximacion al numero PI mediante el metodo de Montecarlo.El parametro 'pasos' indica el numero de puntos generado.
	 * @author Enrique Collado
	 * @param pasos numero de p
	 * @return  devuelve un numero double que se aproxima a PI
	 */


	public static double generarNumeroPi(long pasos){
	int aciertos = 0;
	int areaCuadrado = 4;
	
	for(long i = 1; i<=pasos; i++){
		double x = Math.random()*2-1;
		double y = Math.random()*2-1;
		double distancia = Math.sqrt(x*x+y*y);
		if(distancia<=1){
			aciertos += 1;
		}
	
	}        

	System.out.println(aciertos + " " + pasos);

	double areaCirculo = areaCuadrado*((double)aciertos/pasos);
	double estimacionPi = areaCirculo;
	return estimacionPi;
	}
}
	
	 
	







	

	
	
